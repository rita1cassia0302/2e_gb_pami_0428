import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'detalhes',
    loadChildren: () => import('./detalhes/detalhes.module').then( m => m.DetalhesPageModule)
  },
  {
    path: 'cad-autor',
    loadChildren: () => import('./cad-autor/cad-autor.module').then( m => m.CadAutorPageModule)
  },
  {
    path: 'cad-editora',
    loadChildren: () => import('./cad-editora/cad-editora.module').then( m => m.CadEditoraPageModule)
  },
  {
    path: 'cad-livro',
    loadChildren: () => import('./cad-livro/cad-livro.module').then( m => m.CadLivroPageModule)
  },
  {
    path: 'admin-login',
    loadChildren: () => import('./admin/admin-login/admin-login.module').then( m => m.AdminLoginPageModule)
  },
  {
    path: 'dashboard',
    loadChildren: () => import('./admin/dashboard/dashboard.module').then( m => m.DashboardPageModule)
  },
  {
    path: 'usuarios',
    loadChildren: () => import('./admin/usuarios/usuarios.module').then( m => m.UsuariosPageModule)
  },
  {
    path: 'cad-usuario',
    loadChildren: () => import('./cad-usuario/cad-usuario.module').then( m => m.CadUsuarioPageModule)
  },
  {
    path: 'show-autor',
    loadChildren: () => import('./show-autor/show-autor.module').then( m => m.ShowAutorPageModule)
  },
  {
    path: 'show-editora',
    loadChildren: () => import('./show-editora/show-editora.module').then( m => m.ShowEditoraPageModule)
  },
  {
    path: 'show-livro',
    loadChildren: () => import('./show-livro/show-livro.module').then( m => m.ShowLivroPageModule)
  },
  {
    path: 'show-usuario',
    loadChildren: () => import('./show-usuario/show-usuario.module').then( m => m.ShowUsuarioPageModule)
  },

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
