import { Component, OnInit } from '@angular/core';
import { Autor } from '../models/Autor';
import { AutoresService } from '../services/autores/autores.service';

@Component({
  selector: 'app-cad-autor',
  templateUrl: './cad-autor.page.html',
  styleUrls: ['./cad-autor.page.scss'],
})
export class CadAutorPage implements OnInit {
  private aut: Autor

  constructor(
    private autoresService: AutoresService
  ) { 
    this.aut = new Autor()
  }

  ngOnInit() {
  }

  cadastrar (): void {
    console.log(this.aut)

    this.autoresService.cadastrar(this.aut)
    .subscribe({
      next: (dados) => {
        console.log(dados)
      },
      error: (erro) => {
        console.error(erro)
      }
    })
  }
}
