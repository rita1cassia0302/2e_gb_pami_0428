import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ShowLivroPageRoutingModule } from './show-livro-routing.module';

import { ShowLivroPage } from './show-livro.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ShowLivroPageRoutingModule
  ],
  declarations: [ShowLivroPage]
})
export class ShowLivroPageModule {}
