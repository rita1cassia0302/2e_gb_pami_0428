import { Component, OnInit } from '@angular/core';
import { Livro } from '../models/Livro';
import { LivrosService } from '../services/livros/livros.service';

@Component({
  selector: 'app-show-livro',
  templateUrl: './show-livro.page.html',
  styleUrls: ['./show-livro.page.scss'],
})
export class ShowLivroPage implements OnInit {
  livros: Livro[]

  constructor(
    private livrosService: LivrosService
  ) { }

  ngOnInit() {
    this.livrosService.buscar().subscribe({
      next: (resposta) => {
        this.livros = resposta.results
      },
      error: erro =>{
        console.error(erro)
      }
    })
  }
}
