import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Autor } from 'src/app/models/Autor';

@Injectable({
  providedIn: 'root'
})
export class AutoresService {
  private readonly URL_R = "https://3000-rita1cassia-2egbapi0810-kuyjwiw3zbu.ws-us78.gitpod.io/"
  private readonly URL_V = "https://3000-rita1cassia-2egbapi0810-kuyjwiw3zbu.ws-us79.gitpod.io/"
  private readonly URL = this.URL_V

  constructor(
    private http: HttpClient
  ) { }

  cadastrar(autor: Autor): Observable <any> {
    return this.http.post<any>(`${this.URL}autor`, autor)
  }

  buscar(): Observable <any> {
    return this.http.get<any>(`${this.URL}autores`)
  }
}
