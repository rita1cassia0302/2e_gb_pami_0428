import { Usuario } from './../../models/Usuario';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UsuariosService {
  private readonly URL_R = "https://3000-rita1cassia-2egbapi0810-kuyjwiw3zbu.ws-us78.gitpod.io/"
  private readonly URL_V = "https://3000-rita1cassia-2egbapi0810-kuyjwiw3zbu.ws-us79.gitpod.io/"
  private readonly URL = this.URL_V

  constructor(
    private http: HttpClient
  ){ }

 
  logar(usuario: Usuario): Observable <any> {
    return this.http.post<any>(`${this.URL}usuario/login`, usuario)
  }
  
  cadastrar(usuario: Usuario): Observable <any> {
    return this.http.post<any>(`${this.URL}usuario`, usuario)
  }

  buscar(): Observable <any> {
    return this.http.get<any>(`${this.URL}usuarios`)
  }
}