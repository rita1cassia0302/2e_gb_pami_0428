import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Funcionario } from 'src/app/models/Funcionario';

@Injectable({
  providedIn: 'root'
})
export class FuncionariosService {
  private readonly URL_R = "https://3000-rita1cassia-2egbapi0810-kuyjwiw3zbu.ws-us78.gitpod.io/"
  private readonly URL_V = "https://3000-rita1cassia-2egbapi0810-kuyjwiw3zbu.ws-us78.gitpod.io/"
  private readonly URL = this.URL_V

  constructor(
    private http: HttpClient
  ){}

  cadastrar(funcionario: Funcionario): Observable <any> {
    return this.http.post<any>(`${this.URL}funcionario`, funcionario)
  }
}
