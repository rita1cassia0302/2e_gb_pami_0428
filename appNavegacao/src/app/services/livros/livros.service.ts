import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Livro } from 'src/app/models/Livro';

@Injectable({
  providedIn: 'root'
})
export class LivrosService {
  private readonly URL_R = "https://3000-rita1cassia-2egbapi0810-kuyjwiw3zbu.ws-us78.gitpod.io/"
  private readonly URL_V = "https://3000-rita1cassia-2egbapi0810-kuyjwiw3zbu.ws-us79.gitpod.io/"
  private readonly URL = this.URL_V

  constructor(
    private http: HttpClient
  ) { }

  cadastrar(livro: Livro): Observable <any> {
    return this.http.post<any>(`${this.URL}livro`, livro)
  }

  buscar(): Observable <any> {
    return this.http.get<any>(`${this.URL}livros`)
  }
}
