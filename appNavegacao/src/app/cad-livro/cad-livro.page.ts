import { Component, OnInit } from '@angular/core';
import { Livro } from '../models/Livro';
import { LivrosService } from '../services/livros/livros.service';

@Component({
  selector: 'app-cad-livro',
  templateUrl: './cad-livro.page.html',
  styleUrls: ['./cad-livro.page.scss'],
})
export class CadLivroPage implements OnInit {
  private livro: Livro

  constructor(
    private livrosService: LivrosService
  ) { 
    this.livro = new Livro()
  }

  ngOnInit() {
  }

  cadastrar (): void {
    console.log(this.livro)

    this.livrosService.cadastrar(this.livro)
    .subscribe({
      next: (dados) => {
        console.log(dados)
      },
      error: (erro) => {
        console.error(erro)
      }
    })
  }
}
