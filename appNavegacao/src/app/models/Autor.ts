import { Pessoa } from "./Pessoa";

export class Autor extends Pessoa {
    nome: string;
    telefone: string;
    rua: string;
    numero: string;
    bairro: string;
    cep: string;
    cidade: string;
    uf: string;
    tipo: string;
    nacionalidade: string;
    data_morte: Date;
    biografia: string;
}