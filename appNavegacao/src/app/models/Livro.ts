export class Livro{
    titulo: string
    resumo: string
    descricao: string
    paginas: number
    preco: number
    edicao: number
    idioma: string
    ano_lancamento: number
    id_autor: number
    id_editora: number
}