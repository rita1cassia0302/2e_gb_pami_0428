import { Component, OnInit } from '@angular/core';
import { Editora } from '../models/Editora';
import { EditorasService } from '../services/editoras/editoras.service';

@Component({
  selector: 'app-cad-editora',
  templateUrl: './cad-editora.page.html',
  styleUrls: ['./cad-editora.page.scss'],
})
export class CadEditoraPage implements OnInit {
  private editora: Editora
  
  constructor(
    private editorasService: EditorasService
  ) { 
    this.editora = new Editora()
  }

  ngOnInit() {
  }

  cadastrar (): void {
    console.log(this.editora)

    this.editorasService.cadastrar(this.editora)
    .subscribe({
      next: (dados) => {
        console.log(dados)
      },
      error: (erro) => {
        console.error(erro)
      }
    })
  }
}
