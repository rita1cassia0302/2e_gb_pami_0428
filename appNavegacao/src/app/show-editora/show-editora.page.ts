import { Component, OnInit } from '@angular/core';
import { Editora } from '../models/Editora';
import { EditorasService } from '../services/editoras/editoras.service';

@Component({
  selector: 'app-show-editora',
  templateUrl: './show-editora.page.html',
  styleUrls: ['./show-editora.page.scss'],
})
export class ShowEditoraPage implements OnInit {
  editoras: Editora[]

  constructor(
    private editorasService: EditorasService
  ) { }

  ngOnInit() {
    this.editorasService.buscar().subscribe({
      next: (resposta) => {
        this.editoras = resposta.results
      },
      error: erro =>{
        console.error(erro)
      }
    })
  }
}
