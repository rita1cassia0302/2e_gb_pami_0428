import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ShowEditoraPage } from './show-editora.page';

const routes: Routes = [
  {
    path: '',
    component: ShowEditoraPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ShowEditoraPageRoutingModule {}
