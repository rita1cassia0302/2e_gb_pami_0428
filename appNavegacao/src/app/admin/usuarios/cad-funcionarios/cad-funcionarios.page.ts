import { Component, OnInit } from '@angular/core';
import { Funcionario } from 'src/app/models/Funcionario';
import { FuncionariosService } from 'src/app/services/funcionarios/funcionarios.service';

@Component({
  selector: 'app-cad-funcionarios',
  templateUrl: './cad-funcionarios.page.html',
  styleUrls: ['./cad-funcionarios.page.scss'],
})
export class CadFuncionariosPage implements OnInit {
  private func: Funcionario

  constructor(
    private funcionariosService: FuncionariosService
  ) { 
    this.func = new Funcionario()
  }

  ngOnInit() {
  }
  cadastrar (): void {
    console.log(this.func)

    this.funcionariosService.cadastrar(this.func)
    .subscribe({
      next: (dados) => {
        console.log(dados)
      },
      error: (erro) => {
        console.error(erro)
      }
    })
  }
}
