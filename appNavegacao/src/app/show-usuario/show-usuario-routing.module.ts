import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ShowUsuarioPage } from './show-usuario.page';

const routes: Routes = [
  {
    path: '',
    component: ShowUsuarioPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ShowUsuarioPageRoutingModule {}
