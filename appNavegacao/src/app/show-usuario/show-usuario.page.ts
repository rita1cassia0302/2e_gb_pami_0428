import { Component, OnInit } from '@angular/core';
import { Usuario } from '../models/Usuario';
import { UsuariosService } from '../services/usuarios/usuarios.service';

@Component({
  selector: 'app-show-usuario',
  templateUrl: './show-usuario.page.html',
  styleUrls: ['./show-usuario.page.scss'],
})
export class ShowUsuarioPage implements OnInit {
  usuarios: Usuario[]

  constructor(
    private usuariosService: UsuariosService
  ) { }

  ngOnInit() {
    this.usuariosService.buscar().subscribe({
      next: (resposta) => {
        this.usuarios = resposta.results
      },
      error: erro =>{
        console.error(erro)
      }
    })
  }
}
